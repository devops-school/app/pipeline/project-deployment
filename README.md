# Pipeline for deployments

#### Overview

Run new pipeline to deploy all services (or destroy them) to one of the namespaces:

* milkyway
* andromeda
* sculptor

Also you can start smoke-tests and deploy all services to the production.